# Backend

Run `index.js` file which creates an Express.js application which
responds with the system status (cpu, ram, swap, disk usage, uptime,
load, etc.)

## API

`cpu_used`: percentage of average cpu usage
`ram_used`: percentage of ram used
`swap_used`: percentage of swap used
`disks`: object where its key is the mount point and the value the
disk usage in percentage
`uptime`: time since start in seconds
`load_avg`: array of system load averages (1m, 5m and 15m)
`temperature`: main temperature
