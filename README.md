# Server Dashboard

Quick dashboard for my server with Windows 98 ✨*aesthetics*✨.

![Dashboard screenshot with Windows 98 decorations showing a torrent
client, list of services and system monitor](screenshot.png)

CSS is based on [98.css](https://jdan.github.io/98.css/) with some
modifications (meter bar, progress bar).

## Usage

Clone the repository and modify the `src/config.json` file with the
appropiate values:

- `server_api_url`: location of the API (see backend subfolder)
- `qbittorrent_api_url`: location of the qBittorrent API. Might need
  some tweaking in order to communicate well
- `system_monitor_update_interval_ms`: delay between system monitor
  updates,
- `torrents_update_interval_ms`: delay between torrent status update,
- `max_torrents`: maximum torrents shown at once
- `torrent_name_max_length`: maximum length for each torrent name. If
  length is greater, last 3 characters are removed and replaced by an
  ellipsis
- `services`: array of objects with properties `name`, `icon` and
  `url`.

Then execute install dependencies and execute with `yarn dev` (or
equivalent)
