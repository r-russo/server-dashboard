import config from "./config.json";
import * as utils from "./utils";

const update_system_monitor = () => {
  const monitors = document.getElementById("monitors");
  const status_bar = document.getElementById("status-bar");
  const window_system_monitor = document.getElementById(
    "window-system-monitor"
  );
  fetch(config.server_api_url)
    .then((r) => r.json())
    .then((data) => {
      monitors.replaceChildren(
        utils.create_group("CPU Usage", data.cpu_used),
        utils.create_group("RAM Usage", data.ram_used),
        utils.create_group("Swap Usage", data.swap_used),
        utils.create_group("Root", data.disks["/"]),
        utils.create_group("External", data.disks["/mnt/external"])
      );

      status_bar.replaceChildren(
        utils.create_status(`Uptime: ${utils.seconds_to_time(data.uptime)}`),
        utils.create_status(`System load: ${data.load_avg.join(", ")}`),
        utils.create_status(`Temperature: ${data.temperature} °C`)
      );
      window_system_monitor.style.opacity = "100%";
    });
};

const generate_icons = () => {
  const services = document.getElementById("services");
  const window_services = document.getElementById("window-services");
  for (let item of config.services) {
    const node = document.createElement("a");
    node.className = "service";
    node.href = item.url;
    const img = document.createElement("img");
    img.src = item.icon;
    node.appendChild(img);
    node.appendChild(document.createTextNode(item.name));
    services.appendChild(node);
  }
  window_services.style.opacity = "100%";
};

async function update_torrents_list() {
  const torrents_table = document.getElementById("torrents-table");
  const window_qbittorrent = document.getElementById("window-qbittorrent");

  // need
  // WebUI\CSRFProtection=false from QBittorrent.conf
  // Whitelist IP and Access-Control-Allow-Origin header from UI
  const response = await fetch(
    config.qbittorrent_api_url +
      "/api/v2/torrents/info?sort=added_on&reverse=true"
  );

  if (response.status == 401) {
    // authorize
    return;
  } else if (response.status != 200) {
    return;
  }

  const data = await response.json();

  let table = document.createElement("div");
  table.className = "table-view row";

  let col_name = document.createElement("div");
  col_name.className = "col torrent-name";
  let btn_name = document.createElement("button");
  btn_name.appendChild(document.createTextNode("Name"));
  col_name.appendChild(btn_name);

  let col_status = document.createElement("div");
  col_status.className = "col";
  let btn_status = document.createElement("button");
  btn_status.appendChild(document.createTextNode("Status"));
  col_status.appendChild(btn_status);

  let col_progress = document.createElement("div");
  col_progress.className = "col torrent-progress";
  let btn_progress = document.createElement("button");
  btn_progress.appendChild(document.createTextNode("Progress"));
  col_progress.appendChild(btn_progress);

  let col_size = document.createElement("div");
  col_size.className = "col";
  let btn_size = document.createElement("button");
  btn_size.appendChild(document.createTextNode("Size"));
  col_size.appendChild(btn_size);

  const max_torrents = Math.min(config.max_torrents, data.length);
  for (let i = 0; i < max_torrents; i++) {
    let p_name = document.createElement("p");
    let p_status = document.createElement("p");
    let div_progress = document.createElement("div");
    div_progress.className = "meter";
    let p_size = document.createElement("p");

    let name = data[i].name;
    if (name.length > config.torrent_name_max_length) {
      name = name.substr(0, config.torrent_name_max_length - 3) + "...";
    }

    p_name.appendChild(document.createTextNode(name));
    p_status.appendChild(document.createTextNode(data[i].state));
    p_status.className = utils.status_color(data[i].state);
    let bar = document.createElement("span");
    bar.style = `width: ${Math.round(data[i].progress * 100)}%;`;
    div_progress.appendChild(bar);
    p_size.appendChild(
      document.createTextNode(utils.bytes_to_gb(data[i].size))
    );

    col_name.appendChild(p_name);
    col_status.appendChild(p_status);
    col_progress.appendChild(div_progress);
    col_size.appendChild(p_size);
  }

  table.appendChild(col_name);
  table.appendChild(col_status);
  table.appendChild(col_progress);
  table.appendChild(col_size);
  torrents_table.replaceChildren(table);
  window_qbittorrent.style.opacity = "100%";

  //   fetch(
  //     config.qbittorrent_api_url +
  //       "/api/v2/torrents/info?sort=added_on&reverse=true"
  //   )
  //     .then((r) => {
  //         if (r.status == 200){
  //             return r.json();
  //         }
  //     })
  //     .then((data) => {
  //       let table = document.createElement("div");
  //       table.className = "table-view row";

  //       let col_name = document.createElement("div");
  //       col_name.className = "col torrent-name";
  //       let btn_name = document.createElement("button");
  //       btn_name.appendChild(document.createTextNode("Name"));
  //       col_name.appendChild(btn_name);

  //       let col_status = document.createElement("div");
  //       col_status.className = "col";
  //       let btn_status = document.createElement("button");
  //       btn_status.appendChild(document.createTextNode("Status"));
  //       col_status.appendChild(btn_status);

  //       let col_progress = document.createElement("div");
  //       col_progress.className = "col torrent-progress";
  //       let btn_progress = document.createElement("button");
  //       btn_progress.appendChild(document.createTextNode("Progress"));
  //       col_progress.appendChild(btn_progress);

  //       let col_size = document.createElement("div");
  //       col_size.className = "col";
  //       let btn_size = document.createElement("button");
  //       btn_size.appendChild(document.createTextNode("Size"));
  //       col_size.appendChild(btn_size);

  //       const max_torrents = Math.min(config.max_torrents, data.length);
  //       for (let i = 0; i < max_torrents; i++) {
  //         let p_name = document.createElement("p");
  //         let p_status = document.createElement("p");
  //         let div_progress = document.createElement("div");
  //         div_progress.className = "meter";
  //         let p_size = document.createElement("p");

  //         let name = data[i].name;
  //         if (name.length > config.torrent_name_max_length) {
  //           name = name.substr(0, config.torrent_name_max_length - 3) + "...";
  //         }

  //         p_name.appendChild(document.createTextNode(name));
  //         p_status.appendChild(document.createTextNode(data[i].state));
  //         p_status.className = utils.status_color(data[i].state);
  //         let bar = document.createElement("span");
  //         bar.style = `width: ${Math.round(data[i].progress * 100)}%;`;
  //         div_progress.appendChild(bar);
  //         p_size.appendChild(
  //           document.createTextNode(utils.bytes_to_gb(data[i].size))
  //         );

  //         col_name.appendChild(p_name);
  //         col_status.appendChild(p_status);
  //         col_progress.appendChild(div_progress);
  //         col_size.appendChild(p_size);
  //       }

  //       table.appendChild(col_name);
  //       table.appendChild(col_status);
  //       table.appendChild(col_progress);
  //       table.appendChild(col_size);
  //       torrents_table.replaceChildren(table);
  //       window_qbittorrent.style.opacity = "100%";
  //     });
}

addEventListener("load", (_ev) => {
  // first update

  generate_icons();
  //   update_torrents_list();
  update_system_monitor();

  setInterval(update_system_monitor, config.system_monitor_update_interval_ms);
  //   setInterval(update_torrents_list, config.torrents_update_interval_ms);
});
