const express = require("express");
const cors = require("cors");
const app = express();
const port = 3000;

const si = require("systeminformation");
const os = require("os");
const osu = require("node-os-utils");

app.use(cors());

app.get("/", async function(_req, res) {
    let cpu = await osu.cpu.usage();
    let ram = await si.mem();
    let disks = await si.fsSize();
    let temperature = await si.cpuTemperature();

    let object_of_disks = {};
    for (let disk of disks) {
        object_of_disks[disk.mount] = disk.use;
    }

    res.send({
        cpu_used: cpu,
        ram_used: (ram.available / ram.total) * 100,
        swap_used: (ram.swapused / ram.swaptotal) * 100,
        disks: object_of_disks,
        uptime: os.uptime(),
        load_avg: os.loadavg(),
        temperature: temperature.main,
    });
});

app.listen(port, () => {
    console.log(`System monitor listening on port ${port}`);
});
