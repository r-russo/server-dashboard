export const status_color = (state) => {
    switch (state) {
        case "downloading" || "metaDL" || "forcedDL":
            return "downloading";
        case "uploading" || "forcedUP":
            return "seeding";
        case "error" || "missingFiles":
            return "error";
        default:
            return "paused";
    }
};

export const create_group = (title, data) => {
    let fieldset = document.createElement("fieldset");
    let legend = document.createElement("legend");
    legend.appendChild(document.createTextNode(title));

    let monitor = document.createElement("div");
    monitor.className = "field-row";
    monitor.appendChild(make_usage_bar(data));

    fieldset.appendChild(legend);
    fieldset.appendChild(monitor);
    return fieldset;
};

export const create_status = (content) => {
    let node = document.createElement("p");
    node.className = "status-bar-field";
    node.appendChild(document.createTextNode(content));
    return node;
};

export const bytes_to_gb = (size) =>
    `${(size / 1024 / 1024 / 1024).toFixed(2)} GB`;

export const seconds_to_time = (seconds) => {
    let minutes = Math.floor(seconds / 60);
    if (minutes < 1) {
        return `${seconds}s`;
    }
    let time_str = `${seconds % 60}s`;

    let hours = Math.floor(minutes / 60);
    if (hours < 1) {
        return `${minutes}m ${time_str}`;
    }
    time_str = `${minutes % 60}m ${time_str}`;

    let days = Math.floor(hours / 24);
    if (days < 1) {
        return `${hours}h ${time_str}`;
    }
    time_str = `${days}d ${hours % 24}h ${time_str}`;

    return time_str;
};

const make_usage_bar = (percentage) => {
    let percentage_int = parseInt(percentage);

    if (isNaN(percentage_int)) {
        return document.createElement("div");
    }

    let monitor_meter = document.createElement("div");
    monitor_meter.className = "monitor-meter";

    let monitor_graph = document.createElement("div");
    monitor_graph.className = "monitor-graph";

    let monitor_background = document.createElement("div");
    monitor_background.className = "monitor-background";

    let span_bar = document.createElement("span");
    span_bar.style = `height: ${100 - percentage_int}%;`;

    let span_number = document.createElement("span");
    span_number.textContent = `${percentage_int}%`;

    monitor_background.appendChild(span_bar);
    monitor_graph.appendChild(monitor_background);
    monitor_graph.appendChild(monitor_background.cloneNode(true));

    monitor_meter.appendChild(monitor_graph);
    monitor_meter.appendChild(span_number);

    return monitor_meter;
};
